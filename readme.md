# AT in JS - WebdriverIO Task#3

This is my solution for the WDIO Task#3.

## Prerequisites

1. Git installed
2. NodeJS installed

## Script scope

1. Opens the cloud pricing calculator page
2. Gets a price quote based on the parameters given in the task description
3. Opens Yopmail and generates a random temporary e-mail address
4. Sends the price quote to the temporary e-mail address then checks the inbox for the e-mail