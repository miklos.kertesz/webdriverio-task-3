import HeaderComponent from "../components/header.component.js";

export default class BasePage {

    constructor() {
        this.header = new HeaderComponent();
    }

    open() {
        // using this url because I was not able to interact with any form items
        // using the proper selectors at  
        // https://cloud.google.com/products/calculator-legacy
        // maybe because the whole form is within an iFrame (?)
        return browser.url("https://cloudpricingcalculator.appspot.com/");
    }
}
