import CalculatorFormComponent from "../components/calculatorForm.component.js";
import CostEstimateComponent from "../components/costEstimate.component.js";
import EmailModalWindowComponent from "../components/emailModalWindow.component.js";

export default class CostCalculatorPage {
    constructor() {
        this.calculatorForm = new CalculatorFormComponent();
        this.costEstimate = new CostEstimateComponent();
        this.emailModalWindow = new EmailModalWindowComponent();
    }
}
