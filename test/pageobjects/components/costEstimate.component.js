import BaseComponent from "./base.component.js";

export default class CostEstimateComponent extends BaseComponent {
    constructor() {
        super("#resultBlock");
    }

    item(value) {
        const selectorItems = {
            numberOfInstances: "//*[contains(text(),' x')]",
            location: "//*[contains(text(),'Region:')]",
            commitment: "//*[contains(text(),'Commitment term:')]",
            provisioningModel: "//*[contains(text(),'Provisioning model:')]",
            instanceType: "//*[contains(text(),'Instance type:')]",
            os: "//*[contains(text(),'Operating System / Software:')]",
            gpu: "//*[contains(text(),'GPU dies:')]",
            ssd: "//*[contains(text(),'Local SSD:')]"
        }

        return this.rootEl.$(selectorItems[value]);
    }

    get totalEstimatedCost() {
        return $("//*[contains(text(),'Total Estimated Cost:')]");
    }

    get emailEstimateBtn() {
        return $("//*[@id='Email Estimate']");
    }

}