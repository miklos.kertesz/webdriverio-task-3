import BaseComponent from "../components/base.component.js";

export default class CalculatorFormComponent extends BaseComponent {

    constructor() {
        super("[name='ComputeEngineForm']");
    }

    select(option) {
        const selectorItems = {
            numberOfInstances: "[ng-model='listingCtrl.computeServer.quantity']",
            osSelector: "[ng-model='listingCtrl.computeServer.os']",
            provisioningMode: "[ng-model='listingCtrl.computeServer.class']",
            machineFamily: "[ng-model='listingCtrl.computeServer.family']",
            series: "[ng-model='listingCtrl.computeServer.series']",
            machineType: "[ng-model='listingCtrl.computeServer.instance']",
            gpuType: "[ng-model='listingCtrl.computeServer.gpuType']",
            storage: "[ng-model='listingCtrl.computeServer.ssd']",
            location: "[ng-model='listingCtrl.computeServer.location']",
            gpuCount: "[ng-model='listingCtrl.computeServer.gpuCount']",
            committedUsage: "[ng-model='listingCtrl.computeServer.cud']"
        }

        return this.rootEl.$(selectorItems[option]);
    }

    setOSTo(value) {
        const selectorItems = {
            free: "[value='free']"
        }
        return $(selectorItems[value]);
    }

    setProvisioningModeTo(value) {
        const selectorItems = {
            regular: "[value='regular']"
        }
        return $(selectorItems[value]);
    }

    setMachineFamilyTo(value) {
        const selectorItems = {
            generalPurpose: "[value='gp']"
        }
        return $(selectorItems[value]);
    }

    setMachineSeriesTo(value) {
        const selectorItems = {
            n1: "[value='n1']"
        }
        return $(selectorItems[value]);
    }

    setMachineTypeTo(value) {
        const selectorItems = {
            n1Standard8: "[value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']"
        }
        return $(selectorItems[value]);
    }

    setGPUTypeTo(value) {
        const selectorItems = {
            teslaT4: "[value='NVIDIA_TESLA_T4']" // task description requires V100, but it's not available for Frankfurt location
        }
        return $(selectorItems[value]);
    }

    setGPUQtyTo(value) {
        const selectorItems = {
            oneGPU: "[ng-repeat='item in listingCtrl.supportedGpuNumbers[listingCtrl.computeServer.gpuType]'][value='1']"
        }
        return $(selectorItems[value]);
    }

    setSSDTo(value) {
        const selectorItems = {
            double: "[ng-repeat='item in listingCtrl.dynamicSsd.computeServer'][value='2']"
        }
        return $(selectorItems[value]);
    }

    setLocationTo(value) {
        const selectorItems = {
            frankfurt: "[ng-repeat='item in listingCtrl.fullRegionList | filter:listingCtrl.inputRegionText.computeServer'][value='europe-west3'] .md-text"
        }
        return $(selectorItems[value]);
    }

    setUsageTo(value) {
        const selectorItems = {
            oneYear: "#select_option_138"
        }
        return $(selectorItems[value]);
    }

    get addGPUsCheckbox() {
        return $("[ng-model='listingCtrl.computeServer.addGPUs']");
    }

    get addToEstimateBtn() {
        return $("[ng-click='listingCtrl.addComputeServer(ComputeEngineForm);']")
    }
}