import BasePage from "../pageobjects/pages/base.page.js";
import CostCalculatorPage from "../pageobjects/pages/costCalculator.page.js";

const basePage = new BasePage();
const costCalculatorPage = new CostCalculatorPage();

describe('Google Cloud Pricing Calculator', () => {

    beforeEach(async () => {
        await basePage.open();
        //await browser.switchToWindow($('[src="https://cloud.google.com/frame/products/calculator-legacy/index_d6a98ba38837346d20babc06ff2153b68c2990fa24322fe52c5f83ec3a78c6a0.frame"]'));
    });

    it('Checks page title', async () => {
        await expect(browser).toHaveTitle('Cloud Pricing Calculator');
    });

    it('Gets a price quotation, verify the result and e-mail the quotation', async () => {

        // set instances to 4
        await costCalculatorPage.calculatorForm.select("numberOfInstances").setValue(4);

        // select free os
        await costCalculatorPage.calculatorForm.select("osSelector").click();
        await costCalculatorPage.calculatorForm.setOSTo("free").click();

        // select regular as provisioning model
        await costCalculatorPage.calculatorForm.select("provisioningMode").click();
        await costCalculatorPage.calculatorForm.setProvisioningModeTo("regular").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setProvisioningModeTo("regular").click();

        // select general purpose as machine family
        await costCalculatorPage.calculatorForm.select("machineFamily").click();
        await costCalculatorPage.calculatorForm.setMachineFamilyTo("generalPurpose").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setMachineFamilyTo("generalPurpose").click();

        // select machine series
        await costCalculatorPage.calculatorForm.select("series").click();
        await costCalculatorPage.calculatorForm.setMachineSeriesTo("n1").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setMachineSeriesTo("n1").click();

        // select machine type
        await costCalculatorPage.calculatorForm.select("machineType").click();
        await costCalculatorPage.calculatorForm.setMachineTypeTo("n1Standard8").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setMachineTypeTo("n1Standard8").click();

        // select GPU
        // I select a different GPU model here, as the one specified in our task
        // is not available for the location we have to select later
        await costCalculatorPage.calculatorForm.addGPUsCheckbox.click();
        await costCalculatorPage.calculatorForm.select("gpuType").click();
        await costCalculatorPage.calculatorForm.setGPUTypeTo("teslaT4").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setGPUTypeTo("teslaT4").click();
        await costCalculatorPage.calculatorForm.select("gpuCount").click();
        await costCalculatorPage.calculatorForm.setGPUQtyTo("oneGPU").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setGPUQtyTo("oneGPU").click();

        // set SSD to 2x375 Gb
        await costCalculatorPage.calculatorForm.select("storage").click();
        await costCalculatorPage.calculatorForm.setSSDTo("double").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setSSDTo("double").click();

        // set location to Frankfurt
        await costCalculatorPage.calculatorForm.select("location").click();
        await costCalculatorPage.calculatorForm.setLocationTo("frankfurt").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setLocationTo("frankfurt").click();

        // set commitment to 1 year
        await costCalculatorPage.calculatorForm.select("committedUsage").click();
        await costCalculatorPage.calculatorForm.setUsageTo("oneYear").waitForDisplayed();
        await costCalculatorPage.calculatorForm.setUsageTo("oneYear").click();

        // click add to estimate button
        await costCalculatorPage.calculatorForm.addToEstimateBtn.click();

        // verify automated input results
        await expect(costCalculatorPage.costEstimate.item("numberOfInstances")).toHaveTextContaining("4");
        await expect(costCalculatorPage.costEstimate.item("location")).toHaveTextContaining("Frankfurt");
        await expect(costCalculatorPage.costEstimate.item("commitment")).toHaveTextContaining("1 Year");
        await expect(costCalculatorPage.costEstimate.item("provisioningModel")).toHaveTextContaining("Regular");
        await expect(costCalculatorPage.costEstimate.item("instanceType")).toHaveTextContaining("n1-standard-8");
        await expect(costCalculatorPage.costEstimate.item("os")).toHaveTextContaining("Free");
        await expect(costCalculatorPage.costEstimate.item("gpu")).toHaveTextContaining("1 NVIDIA TESLA T4");
        await expect(costCalculatorPage.costEstimate.item("ssd")).toHaveTextContaining("2x375 GiB");

        // store total estimated cost
        const costForm = await costCalculatorPage.costEstimate.totalEstimatedCost.getValue();

        // task description was changed while I was working on the task,
        // and the e-mail part was removed from it
        // since I was already working on it, I left it in my code :)
        // I'm not sure though I should've used page objects for Yopmail too,
        // which I eventually didn't
        //
        // new tab for Yopmail, creates a new e-mail address and copies it to the calculator

        await browser.newWindow('https://yopmail.com');

        // get a random e-mail address
        await $('a[href="email-generator"]').click();

        // store the e-mail address in a variable
        const tempEmailAddress = await $('div#geny').getText();

        // switch back to the pricing calculator window
        await browser.switchWindow('cloudpricingcalculator.appspot.com');

        // click the email estimate button and add our 
        // temporary email address to the form
        // then submit
        await costCalculatorPage.costEstimate.emailEstimateBtn.click();
        await costCalculatorPage.emailModalWindow.emailInput.setValue(tempEmailAddress);
        await costCalculatorPage.emailModalWindow.submitEmailBtn.click();

        // switch back to yopmail
        await browser.switchWindow('yopmail.com');

        // click check inbox button
        await $("//*[contains(text(),'Check Inbox')]").click();

        // wait for 10 seconds for the e-mail to arrive
        await browser.pause(10000);
        await $("button#refresh").click();

        // wait for 5 seconds to check the inbox
        await browser.pause(5000);

        //const costEmail = await $$('#mail h3')[1];
        //await expect(costEmail).toHaveHTMLContaining(costForm);

    });

});